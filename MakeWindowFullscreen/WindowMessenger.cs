﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace MakeWindowFullscreen
{
    class WindowMessenger : IUserMessenger
    {
        public void ShowError(string message)
        {
            //MessageBox.Show(message, "MakeWindowFullscreen", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK, MessageBoxOptions.None);
            var window = new MessageWindow(message);
            window.ShowDialog();
        }

        public void ShowInfo(string message)
        {
            //MessageBox.Show(message, "MakeWindowFullscreen", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK, MessageBoxOptions.None);
            var window = new MessageWindow(message);
            window.ShowDialog();
        }
    }
}
