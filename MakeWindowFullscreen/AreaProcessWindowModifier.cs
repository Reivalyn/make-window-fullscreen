﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace MakeWindowFullscreen
{
    class AreaProcessWindowModifier : IProcessWindowModifier
    {
        Rectangle area;

        public AreaProcessWindowModifier(Rectangle area)
        {
            this.area = area;
        }

        /// <summary>
        /// Removes the window's frame and set its position and size to the dimensions of the specified area.
        /// </summary>
        /// <param name="window"></param>
        public void Modify(ProcessWindow window)
        {
            window.RemoveFrame();

            window.SetPositionAndSize(
                this.area.Left,
                this.area.Top,
                this.area.Width,
                this.area.Height);
        }
    }
}
