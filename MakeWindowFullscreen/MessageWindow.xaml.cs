﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MakeWindowFullscreen
{
    /// <summary>
    /// Interaction logic for MessageWindow.xaml
    /// </summary>
    public partial class MessageWindow : Window
    {
        private MessageWindow()
        {
            InitializeComponent();
        }

        public MessageWindow(string messageText)
            : this()
        {
            this.Message.Text = messageText;
        }

        public MessageWindow(string messageText, string headerText)
            : this(messageText)
        {
            this.Title = headerText;
        }

        private void CopyMessageToClipboard()
        {
            Clipboard.SetData(DataFormats.Text, this.Message.Text);
        }

        private void Menu_Action_Copy_Click(object sender, RoutedEventArgs e)
        {
            this.CopyMessageToClipboard();
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Escape:
                    this.Close();
                    break;

                case Key.C:
                    if (ModifierKeys.Control == (ModifierKeys.Control & e.KeyboardDevice.Modifiers))
                    {
                        this.CopyMessageToClipboard();
                    }
                    break;
            }
        }
    }
}
