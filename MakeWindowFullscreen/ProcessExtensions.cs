﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

using MakeWindowFullscreen.WinApi;

namespace MakeWindowFullscreen
{
    static class ProcessExtensions
    {
        public static string GetImageName(this Process process)
        {
            if (OSVersions.IsQueryFullProcessImageNameSupported)
            {
                const int SystemIdleProcessId = 0;

                if (process.Id == SystemIdleProcessId)
                {
                    return null;
                }

                IntPtr hprocess = SafeNativeMethods.OpenProcess(ProcessAccessFlags.PROCESS_QUERY_LIMITED_INFORMATION, false, process.Id);

                if (hprocess == IntPtr.Zero)
                {
                    var errorCode = Marshal.GetLastWin32Error();

                    const int AccessDeniedErrorCode = 0x5;
                    if (errorCode == AccessDeniedErrorCode)
                    {
                        return null;
                    }

                    throw new Win32Exception(errorCode, string.Format("Failed to open process {0}: \"{1}\"", process.Id, process.ProcessName));
                }

                try
                {
                    int size = 1024;
                    var buffer = new StringBuilder(size);
                    if (!SafeNativeMethods.QueryFullProcessImageName(hprocess, 0, buffer, ref size))
                    {
                        throw new Win32Exception(Marshal.GetLastWin32Error(), string.Format("Failed to query the image name for process {0}: \"{1}\"", process.Id, process.ProcessName));
                    }

                    return buffer.ToString(0, size);
                }
                finally
                {
                    SafeNativeMethods.CloseHandle(hprocess);
                }
            }
            else
            {
                try
                {
                    return process.MainModule.FileName;
                }
                catch (Win32Exception ex)
                {
                    const uint AccessIsDeniedHResult = 0x80004005;
                    int accessIsDeniedHResult;
                    unchecked
                    {
                        accessIsDeniedHResult = (int)AccessIsDeniedHResult;
                    }
                    if (ex.NativeErrorCode == accessIsDeniedHResult)
                    {
                        return null;
                    }
                    else
                    {
                        throw;
                    }
                }
            }
        }
    }
}
