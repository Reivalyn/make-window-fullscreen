﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MakeWindowFullscreen.WinApi
{
    enum DeviceCap
    {
        /// <summary>
        /// Horizontal width in pixels
        /// </summary>
        HORZRES = 8,

        /// <summary>
        /// Vertical height in pixels
        /// </summary>
        VERTRES = 10,
    }
}
