﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakeWindowFullscreen.WinApi
{
    enum SetWindowPosFlags : uint
    {
        SWP_FRAMECHANGED = 0x0020,
    }
}
