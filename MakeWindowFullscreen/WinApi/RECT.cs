﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace MakeWindowFullscreen.WinApi
{
    [StructLayout(LayoutKind.Sequential)]
    public struct RECT
    {
        private int left;
        private int top;
        private int right;
        private int bottom;

        public RECT(int left, int top, int right, int bottom)
        {
            this.left = left;
            this.top = top;
            this.right = right;
            this.bottom = bottom;
        }

        public int Left
        {
            get { return this.left; }
        }

        public int Top
        {
            get { return this.top; }
        }

        public int Right
        {
            get { return this.right; }
        }

        public int Bottom
        {
            get { return this.bottom; }
        }

        public int X
        {
            get { return this.Left; }
        }

        public int Y
        {
            get { return this.Top; }
        }

        public int Height
        {
            get { return this.Bottom - this.Top; }
        }

        public int Width
        {
            get { return this.Right - this.Left; }
        }

        public static bool operator ==(RECT r1, RECT r2)
        {
            return r1.Equals(r2);
        }

        public static bool operator !=(RECT r1, RECT r2)
        {
            return !r1.Equals(r2);
        }

        public bool Equals(RECT r)
        {
            return r.Left == Left && r.Top == Top && r.Right == Right && r.Bottom == Bottom;
        }

        public override bool Equals(object obj)
        {
            if (obj is RECT)
                return Equals((RECT)obj);
            return false;
        }

        public override int GetHashCode()
        {
            return (this.left ^ this.top ^ this.right ^ this.bottom);
        }

        public override string ToString()
        {
            return string.Format("{{Left:{0},Top:{1},Right:{2},Bottom:{3}}}", Left, Top, Right, Bottom);
        }
    }
}
