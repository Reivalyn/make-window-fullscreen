﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

using MakeWindowFullscreen.WinApi;

namespace MakeWindowFullscreen
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private IUserMessenger messenger = new WindowMessenger();

        private void ShowHelp(string executablePath)
        {
            var executableName = System.IO.Path.GetFileName(executablePath);

            string usageText;

            var assembly = this.GetType().Assembly;
            using (var rs = assembly.GetManifestResourceStream(assembly.GetName().Name + ".Usage.txt"))
            {
                using (var reader = new System.IO.StreamReader(rs))
                {
                    usageText = reader.ReadToEnd();
                }
            }

            var message = string.Format(usageText, executableName);

            this.messenger.ShowInfo(message);
        }

        private void ShowError(string message)
        {
            this.messenger.ShowError(message);
        }

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            IProcessWindowModifier processWindowModifier = null;

            var args = Environment.GetCommandLineArgs();

            if (args != null && args.Length > 1)
            {
                string targetProcessName = null;

                int? targetProcessIndex = null;

                Rectangle? area = null;

                bool showHelp = false;

                bool showLicense = false;

                bool processedArgs = false;

                if (args.Length == 2)
                {
                    switch (args[1].ToUpperInvariant())
                    {
                        case "-L":
                        case "/L":
                        case "-LICENSE":
                        case "/LICENSE":
                        case "-?":
                        case "/?":
                            break;

                        default:
                            targetProcessName = args[1];
                            if (string.IsNullOrEmpty(targetProcessName))
                            {
                                this.ShowError("Expected a process name.");
                                this.Shutdown(1);
                                return;
                            }

                            processedArgs = true;
                            break;
                    }
                }
                else if (args.Length == 6)
                {
                    int left, top, width, height;

                    if (!string.IsNullOrEmpty(args[1])
                        && int.TryParse(args[2], out left)
                        && int.TryParse(args[3], out top)
                        && int.TryParse(args[4], out width)
                        && int.TryParse(args[5], out height))
                    {
                        targetProcessName = args[1];

                        targetProcessIndex = 1;

                        area = new Rectangle(left, top, width, height);

                        processedArgs = true;
                    }
                }

                if (!processedArgs)
                {
                    for (int i = 1; i < args.Length; i++)
                    {
                        switch (args[i].ToUpperInvariant())
                        {
                            case "-A":
                            case "/A":
                            case "-AREA":
                            case "/AREA":
                                if (args.Length <= i + 4)
                                {
                                    this.ShowError(string.Format("Expected left, top, width, and height arguments following the {0} parameter.", args[i]));
                                    this.Shutdown(2);
                                    return;
                                }
                                else
                                {
                                    int left, top, width, height;

                                    if (   int.TryParse(args[i + 1], out left)
                                        && int.TryParse(args[i + 2], out top)
                                        && int.TryParse(args[i + 3], out width)
                                        && int.TryParse(args[i + 4], out height))
                                    {
                                        area = new Rectangle(left, top, width, height);

                                        // assumption is that if the user has specified an area, then they will only want to modify one process, not all of them.
                                        // if the user wants to move and resize windows for multiple processes, then the user should use the index parameter to specify which process to target.
                                        if (!targetProcessIndex.HasValue)
                                            targetProcessIndex = 1;

                                        i += 4;
                                    }
                                    else
                                    {
                                        this.ShowError(string.Format("Expected four numeric values for the left, top, width, and height arguments following the {0} parameter.", args[i]));
                                        this.Shutdown(3);
                                        return;
                                    }
                                }
                                break;

                            case "-I":
                            case "/I":
                            case "-INDEX":
                            case "/INDEX":
                                if (args.Length <= i + 1)
                                {
                                    this.ShowError(string.Format("Expected index argument following the {0} parameter.", args[i]));
                                    this.Shutdown(4);
                                    return;
                                }
                                else
                                {
                                    int value;

                                    if (!int.TryParse(args[i + 1], out value))
                                    {
                                        this.ShowError(string.Format("Expected a numeric value for the index argument following the {0} parameter.", args[i]));
                                        this.Shutdown(5);
                                        return;
                                    }
                                    else
                                    {
                                        if (value < 1)
                                        {
                                            this.ShowError(string.Format("The value of the index argument following the {0} parameter must be a positive number.", args[i]));
                                            this.Shutdown(6);
                                            return;
                                        }

                                        targetProcessIndex = value;

                                        i += 1;
                                    }
                                }
                                break;

                            case "-P":
                            case "/P":
                            case "-PROCESS":
                            case "/PROCESS":
                                if (args.Length <= i + 1)
                                {
                                    this.ShowError(string.Format("Expected a process name argument following the {0} parameter.", args[i]));
                                    this.Shutdown(7);
                                    return;
                                }
                                else
                                {
                                    targetProcessName = args[i + 1];

                                    i += 1;
                                }
                                break;

                            case "-O":
                            case "/O":
                            case "-OUTPUT":
                            case "/OUTPUT":
                                if (args.Length <= i + 1)
                                {
                                    this.ShowError(string.Format("Expected an output mode argument following the {0} parameter.", args[i]));
                                    this.Shutdown(8);
                                    return;
                                }
                                else
                                {
                                    switch (args[i + 1].ToUpperInvariant())
                                    {
                                        case "NONE":
                                            this.messenger = new NullMessenger();
                                            break;

                                        case "WINDOW":
                                            this.messenger = new WindowMessenger();
                                            break;

                                        default:
                                            this.ShowError(string.Format("Unrecognized output mode argument following the {0} parameter; valid values are \"window\" or \"none\".", args[i]));
                                            this.Shutdown(9);
                                            return;
                                    }

                                    i += 1;
                                }
                                break;

                            case "-L":
                            case "/L":
                            case "-LICENSE":
                            case "/LICENSE":
                                showLicense = true;
                                break;

                            case "-?":
                            case "/?":
                                showHelp = true;
                                break;

                            case null:
                            case "":
                                this.ShowError(string.Format("Expected a parameter at index {0}.", i));
                                this.Shutdown(10);
                                return;

                            default:
                                this.ShowError(string.Format("Unrecognized parameter: {0}", args[i]));
                                this.Shutdown(11);
                                return;
                        }
                    }
                }

                if (showHelp)
                {
                    this.ShowHelp(args[0]);
                    this.Shutdown();
                    return;
                }

                if (showLicense)
                {
                    string licenseText;

                    var assembly = this.GetType().Assembly;
                    using (var rs = assembly.GetManifestResourceStream(assembly.GetName().Name + ".License.txt"))
                    {
                        using (var reader = new System.IO.StreamReader(rs))
                        {
                            licenseText = reader.ReadToEnd();
                        }
                    }

                    this.messenger.ShowInfo(licenseText);
                }

                if (area.HasValue)
                    processWindowModifier = new AreaProcessWindowModifier(area.Value);
                else
                    processWindowModifier = new FullscreenProcessWindowModifier();

                if (!string.IsNullOrEmpty(targetProcessName))
                {
                    // find the specific process (or processes) identified by the specified name
                    var processes = Process.GetProcessesByName(targetProcessName).OrderBy(item => item.StartTime);

                    if (!processes.Any())
                    {
                        this.ShowError(string.Format("No process with the name \"{0}\" was found.", targetProcessName));
                        this.Shutdown(13);
                        return;
                    }

                    IEnumerable<Process> targetProcesses;

                    if (targetProcessIndex.HasValue)
                    {
                        var numberOfProcesses = processes.Count();

                        if (numberOfProcesses < targetProcessIndex.Value)
                        {
                            string numericSuffix;

                            var modValue = targetProcessIndex.Value % 100;

                            if (modValue > 10 && modValue < 20)
                                numericSuffix = "th";
                            else
                            {
                                modValue %= 10;

                                if (modValue == 1)
                                    numericSuffix = "st";
                                else if (modValue == 2)
                                    numericSuffix = "nd";
                                else if (modValue == 3)
                                    numericSuffix = "rd";
                                else
                                    numericSuffix = "th";
                            }

                            string numberOfProcessesMessage;
                            switch (numberOfProcesses)
                            {
                                case 0:
                                    numberOfProcessesMessage = "are no processes";
                                    break;
                                case 1:
                                    numberOfProcessesMessage = "is only 1 process";
                                    break;
                                default:
                                    numberOfProcessesMessage = string.Format("are only {0} processes", numberOfProcesses);
                                    break;
                            }

                            this.ShowError(string.Format("Unable to find the {1}{2} process with name \"{0}\"; there {3} with that name.", targetProcessName, targetProcessIndex.Value, numericSuffix, numberOfProcessesMessage));
                            this.Shutdown(14);
                            return;
                        }

                        targetProcesses = processes.Skip(targetProcessIndex.Value - 1).Take(1);
                    }
                    else
                    {
                        targetProcesses = processes;
                    }

                    foreach (var targetProcess in targetProcesses)
                    {
                        foreach (var window in ProcessWindow.GetWindowsForProcess(targetProcess))
                        {
                            processWindowModifier.Modify(window);
                        }
                    }

                    // since a target process name was specified, exit without displaying the UI
                    this.Shutdown();
                }
            }

            if (processWindowModifier == null)
                processWindowModifier = new FullscreenProcessWindowModifier();

            this.MainWindow = new MainWindow(this.messenger, processWindowModifier);
            this.MainWindow.Show();
        }

        private void Application_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            var message = string.Format("An unexpected error occurred: {0}", e.Exception);

            this.messenger.ShowError(message);
        }
    }
}
