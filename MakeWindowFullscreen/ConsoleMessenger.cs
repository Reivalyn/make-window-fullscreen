﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MakeWindowFullscreen
{
    class ConsoleMessenger : IUserMessenger
    {
        private TextWriter errorOutput;
        private TextWriter infoOutput;

        public ConsoleMessenger()
        {
            this.errorOutput = Console.Error;
            this.infoOutput = Console.Out;
        }

        public void ShowError(string message)
        {
            this.errorOutput.WriteLine(message);
        }

        public void ShowInfo(string message)
        {
            this.infoOutput.WriteLine(message);
        }
    }
}
