﻿using System;
using System.Diagnostics;

namespace MakeWindowFullscreen
{
    class ProcessViewModel
    {
        public int ProcessId { get; private set; }

        public string ProcessName { get; private set; }

        public string MainWindowTitle { get; private set; }

        public string ExecutablePath { get; private set; }

        public ProcessViewModel(int id, string name, string mainWindowTitle, string executablePath)
        {
            this.ProcessId = id;
            this.ProcessName = name;
            this.MainWindowTitle = mainWindowTitle;
            this.ExecutablePath = executablePath;
        }
    }
}
